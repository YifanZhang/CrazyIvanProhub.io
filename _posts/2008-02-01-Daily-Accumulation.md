---
layout:     post
title:      "[TFL] Speaking II"
subtitle:   "Daily Accumulation"
date:       2008-02-01 12:00:00
author:     "CrazyIvan"
header-img: "img/post-bg-apple-event-2015.jpg"
catalog:    true
tags:
    - TOEFL
---


**Daily Accumulation**



# Day 1



1.       I will not avoid the tasks of today and charge them to tomorrow for I know that tomorrow never comes.

我不会将今日事拖到明日去做，因为明日永远不会到来。

2.       Save your breath.

省省吧。

3.       He's a man of few words.

他是少言寡语之人。

4.       I got your ass handed to you.

我彻底击败了你。（虐得你屁滚尿流）

5.       There's a world of difference.

霄壤之别。

6.       The errors are too many to enumerate.
 错误太多，不胜枚举。

7.       Make a long story short.
 长话短说。

8.       As time goes on.
 随着时间的推移。

9.       I suggest you to wonder a bit on this topic.
 我建议你稍微考虑一下这件事。

10.    I won't abase myself to no purpose.
 我不会徒劳无益地让自己蒙羞。

11.    I have not yet entered my prime.
 我还没有进入状态。

12.    We should value the cultivation of language intuition.
 我们应该重视语感塑造。

13.    He always keeps a complete poker face.
 他总是摆出一幅死板的面孔。
 poker face 扑克脸；一脸正经的面容

14.    Don't speak in such abstract terms.
 不要这样抽象地谈问题。

15.    He's at the end of his resources.
 他束手无策了。

16.    You must make a clean break with him.
 你必须和他划清界线。
 make a clean break with 一刀两断；彻底决裂

17.    Your decision to be, have and do something out of ordinary entails facing difficulties that are out of the ordinary as well.
 你非凡决心或目标将会招致面对超乎寻常的困难。

18.    Prepare for a rainy day.
 未雨绸缪。

19.    He's really just tilting at windmills.
 他在庸人自扰。

20.    We are in the same boat.
 我们通病相连。



**Daily Accumulation**



# Day 2



1.       I want to put a bug in your ear.
 我要提醒你一点。

2.       Throw the helve after the hatchet.
 赔了夫人又折兵。

3.       It is never wise to put all your eggs in one basket.
 孤注一掷是不明智的。

4.       A good anvil does not fear the hammer.
 真金不怕火炼。

5.       Haughtiness invites disaster, humility receives benefit.
 满招损，谦受益。

6.       When it rains, it pours.
 屋漏偏逢连夜雨。

7.       You've come in the nick of time!
 你来得正是时候。

8.       Going too far is as bad as not going far enough.
 过犹不及。

9.       I'm not worthy to touch the hem of her garment.
 我对她是望尘莫及。

10.    No weal without woe.
 福祸相依。

11.    I guess I'm still wet behind the ears.
 我觉得我还是太嫩了一点。

12.    He asked her to marry him on the spur of the moment.
 他一时冲动向她求婚了。
 on the spur of the moment 不加考虑；冲动之下

13.    The foreign bond fell on evil days.
 外国的债券遭到了厄运。
 evil days 苦难的岁月；难熬的日子

14.    His sudden death came as a bolt from the blue.
 他去世的消息犹如晴天霹雳。
 a bolt from the blue 意外事件；晴天霹雳；飞来横祸

15.    I'm between the devil and the deep blue sea.
 我进退两难。
 between the devil and the deep blue sea 左右为难；进退维谷

16.    Such a chance comes once in a blue moon.
 这是个千载难逢的机会。
 once in a blue moon 千载难逢；极为罕见

17.    There he sat for hours in a brown study.
 他坐在那里沉思的许久。

in a brown study 沉思，呆想；幻想

18.    That's a horse of a different color.
 那是另一码事。

19.    The action is a leap in the dark.
 这是个冒险的行动。
 a leap in the dark 轻举妄动；虚无缥缈

20.    I'm green with envy.
 我羡慕极了。



**Daily Accumulation**



# Day 3



1.       The girl is in the pink of health.
 这个女孩非常健康。

2.       Those born to the purple are destined to live in the public eye.
 那些出身贵族的人们的一言一行势必要受到公众的注意。
 born to the purple 出身贵族；出身王室
 in the public eye 广为人知；受人瞩目

3.       His face was purple with rage.
 他气得脸色发紫。

4.       To call black white.
 颠倒黑白。

5.       Birds of a feather flock together.
 人以群分；物以类聚。

6.       You can kill two birds with one stone.
 一石二鸟。

7.       My salary is chicken feed compared with hers.
 和她相比，我的薪水少得可怜。
 chicken feed 微薄的薪水，零钱；家禽饲料

8.       Don't count one's chickens before they are hatched.
 不要高兴得过早。

9.       You have no right to play ducks and drakes with money that has been entrusted to you.
 你没有权利挥霍托你保管的钱。
 play ducks and drakes 打水漂儿；挥霍浪费

10.    She really cooked your goose.
 她真是把你害惨了。

11.    Kill not the goose that lays the golden eggs.
 不要杀鸡取卵。

12.    He is a typical hen-pecked husband.
 他是典型的妻管严。

13.    He used to rise with the lark.
 他过去习惯早起。

14.    We were having a picnic, when suddenly there came a downpour, which was rather a fly in the ointment.
 我们正在野餐，突然倾盆大雨，未免大煞风景。
 a fly in the ointment 美中不足之处；煞风景的事情；使人扫兴的（小）事

15.    His criticisms of the president stirred up a hornet's nest.
 他对董事长的批评导致众怒。
 stir up a hornet's nest 捅了马蜂窝

16.    Any statement against the government is like a red rag to a bull to that minister.
 任何反对政府的言论都会惹得那位部长暴跳如雷。
 like a red rag to a bull 惹人生气的事

17.    I let the cat out of the bag with only one day to go.
 只剩最后一天了，我却泄露了秘密。
 let the cat out of the bag 泄露秘密

18.    You can't teach an old dog new tricks.
 朽木不可雕。

19.    The only rule of the market place was dog-eat-dog.
 市场上的唯一规则就是损人利己。
 dog-eat-dog 自相残杀；狗咬狗；残酷的

20.    When he saw us, he began to put on the dog.
 他一看见我们就开始摆架子。
 put on the dog 摆架子；装腔作势





**Daily Accumulation**



# Day 4



1.       She hasn't a dog's chance of winning the game.
 她毫无机会赢得比赛。
 a dog's chance 极其有限的一点机会（常用于否定句）

2.       Don't shed crocodile tears to me.
 别在我面前假惺惺的了。

3.       I'm not going with you, I have other fish to fry.
 我不和你一起去了，我还有别的事。

4.       Neither fish nor flesh.
 非驴非马。

5.       To teach a fish how to swim.
 关公面前耍大刀。

6.       One can't run with the hare and hunt with the hounds.
 人不能脚踩两只船。

7.       Beard the lion in his den.
 太岁头上动土。

8.       He is a wolf in sheep's clothing, outwardly kind but inwardly vicious.
 他是个披着羊皮的狼，面善心狠。

9.       I'll never cast pearls before swine.
 我绝不会对牛弹琴。

10.    Just play it by ear.
 只要随机应变就行了。

11.    I'm up to my ears in work at the moment.
 我现在工作忙极了。
 up to my ears 忙极了

12.    An eye for an eye.
 以牙还牙。

13.    You really have an eye for beauty.
 你真有审美眼光。

14.    I don't see eye to eye with him.
 我跟他合不来。
 see eye to eye 看法一致；意见相同

15.    That can be seen with half an eye.
 约略一看就一目了然。
 with half an eye 一目了然

16.    It's no good trying to pull the wool over his eyes. He's far too perceptive.
 无法蒙骗他，他的洞察力太强了。
 pull the wool over sb.'s eyes 掩人耳目；蒙蔽某人

17.    He was forced to face up to the situation.
 他只好勇敢地面对这种局面。

18.    You order the dishes and I'll foot the bill.
 你点餐，我买单。

19.    We try to put our best foot forward.
 我们设法展示自己好的一面。
 put one's best foot forward 全力以赴；飞奔；留下一个好印象

20.    Now is the time to put your foot down.
 现在是你坚持自己立场的时候了。
 put one's foot down 坚决反对；坚持立场；果断行动



**Daily Accumulation**



# Day 5



1.       He planned to participate in the demonstration, but then he got cold feet.
 他打算参加示威游行，但是临场退缩了。
 cold feet 害怕，胆怯；信心丧失

2.       If you want to be good at tennis, you must keep your hand in.
 如果想打好网球，就要不断练习。
 keep one's hand in 不间断练习

3.       They live from hand to mouth.
 他们过着勉强糊口的生活。
 live from hand to mouth 勉强糊口

4.       A good artist should have his head in the clouds sometimes, but his feet always on the ground.
 一个好的艺术家应该富于幻想，但始终脚踏实地。
 have one's head in the clouds 想入非非；心不在焉

5.       I can climb in this window, if someone gives me a leg up.
 如果有人助我一臂之力，我能爬进这个窗户。
 give one a leg up 助某人一臂之力

6.       He hasn't a leg to stand on for his behavior.
 他的行为是完全没有道理的。
 not have a leg to stand on 站不住脚；无合理解释（常用于否定句）

7.       It's on the door, as plain as the nose on my face!
 它在门上，像我脸上的鼻子一样明显!

8.       He was cutting off his nose to spite his face.
 他发脾气的时候害了自己。
 to cut off one's nose to spite one's face 损人不利己

9.       The ring cost me an arm and a leg.
 这个戒指花了我一大笔钱。

10.    He has no back bone. When things get difficult, he gives in.
 他性格软弱。一遇到困难就放弃。
 have no back bone 性情懦弱；没有骨气

11.    He certainly has his back to the wall for he has lost his job.
 他情况不妙了，因为他刚刚失业。
 have one's back to the wall 处境困难；困兽之斗

12.    I must help her. After all, she's my own flesh and blood.
 我必须帮助她。她毕竟是我的亲人。
 own flesh and blood 亲人；骨肉同胞

13.    It made my blood run cold.
 令人毛骨悚然。

14.    She had to bone up on the property laws before her big day in court.
 在上庭这个重要日子之前，她必须苦读财产法。
 bone up on 专心致志于；钻研

15.    By this means she cast in a bone between the wife and husband.
 他用这件事离间这对夫妻。
 cast in a bone between 离间；在...之间挑起争端

16.    I have to tighten my belt this week.
 这星期我不得不勒紧裤腰带生活。

17.    I cannot split hairs on that burning query.
 我不能对那个严重的问题说长道短。
 split hairs 斤斤计较；吹毛求疵

18.    She gave me the cold shoulder.
 她对我很冷淡。

19.    Their excuses cut no ice with people.
 他们的辩解无法令人信服。
 cut no ice 无效；没有意义；一无所成

20.    It's completely ruined, so we'll have to start from scratch.
 它完全毁了，我们只好从头做起。
 from scratch 白手起家；从头做起



**Daily Accumulation**



# Day 6



1.       She set the ball rolling.
 她开始谈起话来。

2.       If I take your medicine, I'll definitely kick the bucket.
 如果我吃了你的药，我会死掉的。
 kick the bucket 一命呜呼；翘辫子

3.       A blessing in disguise.
 塞翁失马；因祸得福

4.       Men's knickers went out of style and are now a drug on the market.
 男士灯笼裤已经不时兴了，成了滞销货。
 a drug on the market 滞销商品

5.       He did write a wonderful book, but it was just a flash in the pan.
 他确实写出了一部佳作，但那只是昙花一现。
 a flash in the pan 昙花一现

6.       I felt as fit as a fiddle after a hot bath.
 洗完热水澡我感觉浑身舒畅。
 as fit as a fiddle 非常健康；强壮无比

7.       Perhaps he calculated that he had burned his bridge behind him.
 可能他感到自己已经无路可退，破釜沉舟了。
 burn one's bridge behind one 破釜沉舟

8.       The inquiry is by no means cut and dried.
 调查之事远未盖棺定论。
 cut and dried 老生常谈；已成定局

9.       We will hand on the torch which he has handed to us.
 我们要将文化知识的火炬传承下去。
 hand on the torch 传递火炬；将文化知识的火把传承下去（源于古希腊火炬赛跑）

10.    You cannot have it both ways. You must either stay home or come with us.
 你不可两者兼得。要么待在家里，要么跟我们走。
 have it both ways 两全其美；见风使舵；脚踏两只船

11.    You can cut it both ways.
 你不妨两面兼顾。

12.    To stab someone in the back.
 暗箭伤人。

13.    A watched pot never boils.
 心急吃不了热豆腐。

14.    A word to the wise is sufficient.
 心有灵犀一点通。

15.    It really gets my goat.
 这真叫我发火。

16.    It takes two to tango.
 一个巴掌拍不响。

17.    Like father, like son.
 有其父，必有其子。

18.    Little leaks sink the ship.
 千里之堤毁于蝼蚁。

19.    Money doesn't grow on trees.
 钱不是白来的。

20.    One man's meat is another man's poison.
 众口难调；各有所好



**Daily Accumulation**



# Day 7



1.       Out of sight, out of mind.
 眼不见，心不烦。

2.       That makes two of us.
 我跟你有同感。

3.       The die is cast.
 木已成舟；已成定局

4.       There is truth in wine.
 酒后吐真言。

5.       You mustn't put it off any longer, remember tomorrow never comes.
 不要再拖了，切记不要依赖明天。

6.       To cut a long story short, I decided to stay.
 长话短说，我决定留下来了。
 to cut a long story short 长话短说；简而言之

7.       In the long run prices are bound to rise.
 长远来看，物价肯定要涨。
 in the long run 长远来看；终究

8.       I'm sure you two will get on like a house on fire.
 我敢肯定，你两会一见如故。
 get on like a house on fire 一见如故

9.       There is no smoke without fire.
 无风不起浪。

10.    Those who play with fire will burn themselves.
 玩火者必自焚。
 play with fire 玩火；冒险；轻举妄动

11.    With these people he'd feel like a fish out of water.
 他这些人在一起，他感到很不自在。
 a fish out of water 如鱼离水；不得其所；浑身不自在

12.    They were clearly seeking to fish in troubled waters.
 他们明显就是想要浑水摸鱼。
 to fish in troubled waters 趁火打劫；浑水摸鱼

13.    The argument does not hold water.
 这个论点根本站不住脚。
 hold water 合情合理；站得住脚

14.    A bird in hand is worth two in the bush.
 一鸟在手胜过两鸟在林。

15.    A fall into the pit, a gain in your wit.
 吃一堑，长一智

16.    A rolling stone gathers no moss.
 滚石不生苔，转业不聚财。

17.    Blood is thicker than water, and Bill was part of the dynasty.
 血浓于水，比尔终究是王朝的一员。

18.    Constant dripping wears away the stone.
 滴水穿石。

19.    Distant hills are greener.
 这山望着那山高。

20.    When in Rome do as the Romans do.
 入乡随俗。



**Daily Accumulation**



# Day 8



1.       This alone stamps him as a swindler.
 仅此一点就证明他是一个骗子。
 this alone 仅此而已；仅凭这一点

2.       Let me not defer or neglect it, for I shall not pass this way again.
 不要迟疑，不要怠慢，因为人只能活一次。
 will pass this way again 还会旧地重游

3.       We had only two weeks to tour Malaysia, which was hardly enough time to scratch the surface.
 我们只有两周时间游览马来西亚，连走马观花都来不及。
 scratch the surface 浅尝辄止；触及表面；未做深入研究

4.       They travelled like a blue streak through Italy.
 他们走马观花般地走遍意大利。
 a blue streak 一闪而过的东西；连珠炮似的讲话；风驰电掣

5.       Focus on the certifications that give you the most bang for the buck.
 将注意力放在那些能够给你带来最大收益的认证上。
 the most bang for the buck 最划算的；最大收益

6.       As long as there's a nail-biter, that final ad may be poised to score a touchdown.
 只要局势紧张，最后的广告就准备在最后触地得分的时候播出。
 be poised to 信誓旦旦；准备就绪

7.       Straight to the point.
 开门见山。

8.       Cut through all the crap.
 不兜圈子。

9.       Empty vessels make the most sounds.
 满罐水不响，半罐响叮当。

10.    Enough is as good as a feast.
 知足常乐。

11.    Even Homer sometimes nods.
 智者千虑，必有一失。

12.    Every cloud has a silver lining.
 天无绝人之路。

13.    Great minds think alike.
 英雄所见略同。

14.    He who laughs last laughs best.
 谁笑到最后，谁笑得最好。

15.    It is easy to be wise after the event.
 事后诸葛亮人人都会做。

16.    As the saying goes, it is no use crying over spilt milk.
 俗话说，“覆水难收”。

17.    Look before you leap.
 三思而后行。

18.    Make hay while the sun shines.
 趁热打铁。

19.    Man proposes, God disposes.
 谋事在人，成事在天。

20.    Many a little makes a mickle.
 积少成多。



**Daily Accumulation**



# Day 9



1.       My will is the very least as strong as yours.
 我的意志跟诸位的一样坚定。
 the very least 至少

2.       Money is the root of all evil.
 金钱是万恶之源。

3.       More haste, less speed.
 欲速则不达。

4.       Nothing ventured, nothing gained.
 不入虎穴，焉得虎子。

5.       Once bitten, twice shy.
 一朝被蛇咬，十年怕井绳。

6.       Prevention is better than cure.
 未雨绸缪。

7.       Pride goes before a fall.
 骄傲使人落后。

8.       Speech is silver, silence is gold.
 言语是银，沉默世纪。

9.       The leopard never changes its spots.
 江山易改，本性难移。

10.    There is many a slip between cup and lip.
 凡事难十拿九稳。

11.    Time and tide wait for no man.
 时不我与，时不我待。

12.    To err is human.
 人非圣贤，孰能无过。

13.    Too many cooks spoil the broth.
 人多反误事。

14.    Two wrongs don't make a right.
 不能用被人的错掩饰自己的错。

15.    You have to learn to walk before you can run.
 奔跑之前必须学会走。

16.    Well begun is half done.
 好的开始是成功的一半。

17.    You can't make bricks without straw.
 巧妇难为无米之炊。

18.    You can take a horse to water, but you can't make it drink.
 老牛不喝水，不能强按头。

19.    You can't tell a book by its cover.
 不能以貌取人。

20.    You can't make a silk purse out of a sow's ear.
 朽木不可雕。



**Daily Accumulation**



# Day 10



1.       If it ain't broke, don't fix it!
 不破不修。

2.       Pay somebody back in his/her own coin.
 以其人之道，还治其人之身。

3.       He was left out in the cold at school because he didn't like sports.
 他在学校受冷遇，因为他不爱运动。

4.       I don't want to do this, but I think I'd better go through the motions.
 我不想做这件事，但我还是装装样子比较好。

5.       He used the party funds to feather his own nest.
 他用党的基金中饱私囊。

6.       Everyone has a cross to bear.
 每个人都有自己的苦难。

7.       He is too cocky, let's cut him down to size.
 他太骄傲了，让我们揭穿他的底细。

8.       Rob Peter to pay Paul.
 拆了东墙补西墙。

9.       This court case could open a Pandora's box of similar claims.
 这宗诉讼案会为类似的索赔开启潘多拉魔盒。

10.    Jack of all trades and master of none.
 行行皆通，样样稀松。

11.    I won't go back on my word.
 我不会食言。

12.    If this is really true, it'll take a load off my mind.
 果真如此，我就放心了。

13.    Let's beat our brains out to find a solution.
 让我们动动脑筋想个办法。

14.    They think of themselves as the salt of the earth.
 他们自以为是社会的中坚。

15.    I am not a soft touch.
 我可不是轻易借钱给别人的人。

16.    He has been made a scapegoat for the company's failures.
 他成了公司倒闭的替罪羊。

17.    Every brave man is a man of his word.
 勇敢者不食言。

18.    Peter is nothing but a fly on the wheel.
 彼得无非是妄自尊大的人而已。

19.    Millions of people are all of one mind.
 万众一心。

20.    She is in two minds about it.
 她对此举棋不定。



**Daily Accumulation**



# Day 11



1.       Two heads are better than one.
 集思广益。

2.       She is second to none among all the graduates.
 他毕业时是顶呱呱的一个。

3.       By then, all the witness were six feet under.
 到那时，所有的目击者都埋在黄土之下了。

4.       Six of one and half a dozen of the other.
 半斤对八两。

5.       Nine times out of ten, he will succeed in the examination.
 十有八九，他会考试及格的。

6.       He's not going to let it go down the drain now.
 他不会让它白白丧失掉。

7.       He is on pins and needles.
 他如坐针毡。

8.       I've never seen a company washing dirty linen in public this way.
 我从未见过一个公司这样揭自己的短。

9.       His life was hanging by a thread.
 他的生命岌岌可危。

10.    Rob's always late if he gets here on time, I'll eat my hat.
 鲍勃总迟到，他要是准时到，我把脑袋给你。

11.    He'll probably find himself in pocket in a good year.
 年头好时，他会发现自己赚钱。

12.    I don't want you to end up out of pocket.
 我不希望你最后亏本。

13.    He was born with a silver spoon in his mouth.
 他是嘴巴含着银汤食出生的。

14.    The food is pleasing to my taste.
 这饭菜正合我的口味。

15.    I have to bring home the bacon.
 我不得不养家糊口。

16.    Your wedding present was the icing on the cake.
 您送来的结婚贺礼是锦上添花。

17.    He rests on his laurels.
 他安于已得名誉。

18.    There's a straw in the wind indicating that peace is around the corner.
 有迹象表明，和平即将来临。

19.    Agues come on horseback, but go away on foot.
 病来如山倒，病去如抽丝。

20.    It takes two to make a quarrel.
 一个巴掌拍不响。



**Daily Accumulation**



# Day 12



1.       Don't bite off more than you can chew.
 不要贪多嚼不烂。

2.       It has been difficult to have the best of both worlds.
 想要两全其美很难。

3.       A mind bent on returning is like an arrow.
 归心似箭。

4.       A man of great wisdom often appears slow.
 大智若愚。

5.       Many small victories add up to a big one.
 积少成多。

6.       Those who fly too high may come to grief.
 乐极生悲。

7.       We need to take the gloves off.
 不要心慈手软。

8.       It is just like looking for a needle in a haystack.
 大海捞针。

9.       You must keep an ear to the ground these days.
 近几天你要保持警惕。

10.    I have the first prize in my back pocket.
 头等奖已是我囊中之物了。

11.    It's like pulling hen's teeth to make anything grow in this poor soil.
 让这些土地长东西简直难于登天。

12.    I just lift a finger.
 举手之劳。

13.    Don't make a mountain out of a molehill.
 别小题大做。

14.    She let the cat out of the bag.
 他泄露了秘密。

15.    He's still on thin ice.
 他如履薄冰。

16.    All that is now just water under the bridge.
 一切都付之东流。

17.    Money will come and go.
 钱乃身外之物。

18.    After suffering comes happiness.
 苦尽甘来。

19.    You will reap what you're sowing today.
 你将收获今日播下的种子。

20.    Since now is just about living death.
 生不如死。



**Daily Accumulation**



# Day 13



1.       Come hell or high water.
 就算天崩地裂；无论如何

2.       It's a cross I have to bear.
 这是我应背负的苦难。

3.       It is oil on the flames.
 这是火上浇油。

4.       He cut his own throat by being nasty to the boss.
 他对上司不礼貌是自讨苦吃。

5.       Light travels like an arrow, and time like a shuttle.
 光阴似箭，岁月如梭。

6.       It goes without saying that health is above wealth.
 健康胜于财富是不言而喻的。

7.       This is how I struck up an acquaintance with her.
 这就是我如何遇见她的。

8.       I have never known him behave other than selfishly.
 我知道他一向自私自利。

9.       Fire away.
 开始讲话。

10.    The modest receive benefit, while the conceited reap failure.
 满招损，谦受益。

11.    I just need you to be sure that you're locked in.
 我想让你确认是否参与。

12.    Let one's guard down.
 掉以轻心。

13.    Please feel assured that we will abide by our promise.
 请放心，我们一定会遵守诺言。

14.    Most of her poems abound in imagery.
 他的诗歌大多赋予形象。

15.    I couldn't account for the lump in my throat as I told him the news.
 我告诉他那个消息时，不知为何喉咙哽咽了。

16.    And you must accustom yourself to be more at ease when you are with people.
 当和别人在一起时，要让自己放松。

17.    Hopefully, his punishment will act as a deterrent to others.
 对他的惩罚但愿能起到杀一儆百的作用。

18.    Many small victories add up to a big one.
 积小胜成大胜。

19.    We must adhere to the principle of making study serve the practical purpose.
 我们必须坚持学以致用的原则。

20.    I wish I could afford to dine off fresh meat every day.
 我要是能够天天吃上新鲜的肉就好了。



**Daily Accumulation**



# Day 14



1.       It was unwise of you to agree to that.
 你同意那件事是愚蠢的。

2.       We must allow for human error.
 我们必须体谅人的过失。

3.       Wet days alternate with fine days.
 雨天和晴天相间而来。

4.       You will have to answer for your rush and carelessness.
 你这样毛躁，非吃苦头不可。

5.       He's very learned but rather absent minded.
 他很有学问，可是好忘事。

6.       There is no clear-cut answer to this question.
 这个问题没有确切的答案。

7.       There is a deep-seated conservatism running through our society.
 我们的社会中普遍存在保守思想。

8.       You won't credit her far-fetched story.
 你不会相信她胡扯的话。

9.       I felt duty-bound to help him.
 我觉得帮助他责无旁贷。

10.    The intensely critical spirit is often the narrow-minded and ignorant one.
 极爱吹毛求疵的人通常都是心胸狭窄和无知的人。

11.    The panic-stricken fled in all directions.
 惊慌的人群向四面八方跑去。

12.    Only a short-sighted man will lose sight of the importance of education.
 只有鼠目寸光的人才会看不见教育的重要性。

13.    What a precious muddle-headed chap you are!
 你真是个十足的糊涂虫。

14.    Be careful to avoid being subjective and one-sided.
 切记主观片面。

15.    Is your overcoat made-to-order or ready-made?
 你的外套是定制的还是现成的？

16.    I told him his idea was not exactly epoch-making.
 我告诉他，说他的思想不是划时代的。

17.    Such an earthshaking change must have far-reaching effects.
 像这样翻天覆地的变化一定会产生深远的影响。

18.    They are on the brink of resolving their long-standing dispute over money.
 他们很快就能解决长期以来的金钱纠纷。

19.    Housework is a never-ending task.
 这家务活做起来真是没完没了。

20.    I find his manner very off-putting.
 我觉得他的举止令人厌恶。

 **Daily Accumulation**



# Day 15



1.       She's very well-meaning, but she only makes the situation worse.
 她虽然出于好心，但是却帮了倒忙。

2.       Full-scale reconstruction is under way.
 百废俱兴。

3.       I will pledge myself to a top secret.
 我发誓保守这一秘密。

4.       They are assured of an enjoyable and trouble-free holiday.
 他们肯定会过一个愉快的，无忧无虑的假期。

5.       Is there a duty-free shop?
 这有免税店吗？

6.       The loan is interest free.
 这个贷款是无息的。

7.       He is quite run down and needs a vacation.
 他的身体太虚弱，需要假期。

8.       He has a laid-back attitude to life.
 他有悠然自得的生活态度。

9.       He struck me while I was off guard.
 他趁我不备打了我。

10.    I do it in my off hour.
 我是在空闲的时候做这件事的。

11.    I'm talking off hand, simply speaking my mind.
 我这是信口开河，随便说说想的东西。

12.    I often go to the gym at off peak hours.
 我经常在人少的时候去健身中心。

13.    We are sure to win the match so long as we go all out.
 只要我们全力以赴，一定能赢得比赛。

14.    What she said was an out-and-out lie.
 她说的是个弥天大谎。

15.    She's a good all-round player.
 她是个优秀的全能选手。

16.    Fools never know when they are well off.
 人若愚痴，有福不知。

17.    Is the stock market smoking hot or just smoking something.
 股市现在是一片红火，还是一塌糊涂。

18.    You must not believe an ex-parte statement.
 你不能相信片面之词。

19.    He often goes to the exclusive restaurants.
 他经常去高级饭店。

20.    The two sides are well-matched.
 双方势均力敌。



**Daily Accumulation**



# Day 16



1.       She burst into a tempestuous fit of anger.
 她勃然大怒。

2.       It is advisable to revise the plan.
 修改计划是明智的。

3.       It's my privilege to serve you.
 为你服务是我的荣幸。

4.       It was great fun to have a picnic there.
 在那里野餐很有意思。

5.       It's a shame to deceive your friends.
 骗你自己的朋友是可耻的。

6.       How long does it take to fly across the Pacific Ocean?
 飞渡太平洋要多少时间？

7.       It requires patience to be a teacher.
 当老师需要耐心。

8.       It amused me to hear these jokes.
 听到这些笑话我很开心。

9.       It would take ages to really master a language.
 要掌握一种语言需要花很长时间。

10.    What harm can it do to give advice?
 给人出主意有什么不好？

11.    It's against my principles to collaborate with them.
 和他们合作违反我的原则。

12.    It's beyond me to explain these.
 要解释这些我力不能及。

13.    It's not within my power to change his view.
 要改变他的观点非我所能。

14.    It would be beneath him to accept such a job.
 接受这份工作有失他的身份。

15.    It is just like you to be always ready to help a friend.
 随时准备帮助朋友是你的本色。

16.    According to superstition, breaking a mirror brings bad luck.
 按照迷信的说法，摔掉镜子会带来厄运。

17.    He has a glittering career ahead of him.
 他前程似锦。

18.    I refused to go along with their pathetic charade.
 我拒绝跟他们一样摆出那副可悲的姿态。

19.    There can be no knowledge apart from practice.
 离开实践就没有知识。

20.    We were arm in arm in this work.
 我们在工作中协力，相互配合。



**Daily Accumulation**



# Day 17



1.       As for science, we should do our best to catch up with the world's highest level.
 至于科学，我们要努力赶上世界水平。

2.       The present agreement take effects as from today.
 本协议即日起生效。

3.       As regards economic issues, he agreed with our view.
 在经济问题上，他同意我的看法。

4.       We were completely in the dark as to his future plans.
 对他未来的计划我们全然不知。

5.       She worked for six hours at a stretch.
 她连续工作了6个小时。

6.       How much can you knock back at a time?
 你一次能喝多少？

7.       I've searched up hill and down dale.
 我已经到处找过了。

8.       One cannot stand without credit.
 人无信不立。

9.       Patience is his strong suit.
 忍耐是他的长处。

10.    A slow sparrow should make an early start.
 笨鸟先飞。

11.    He spent a white night.
 他度过了一个不眠夜。

12.    Once on shore, one prays no more.
 好了伤疤忘了痛。

13.    I appeal to all like-minded people to support me.
 我呼吁所有志同道合的人来支持我。

14.    These principles apply to learning math.
 这些原则适用于学习数学。

15.    I am not personally acquainted with her.
 我和她没有私交。

16.    He'll be pleased to hear you ask after him.
 他听到你的问候一定会很高兴。

17.    He had the audacity to ask for an increase in salary.
 他竟然厚着脸皮要求加薪水。

18.    He suffered the humiliation of being forced to ask for his cards.
 他蒙受了被迫要求辞职的耻辱。

19.    I fully assent to your plan.
 我完全同意你的计划。

20.    Never associate with bad companions.
 别和坏人结交。



**Daily Accumulation**



# Day 18



1.       She assumed an air of confidence in spite of her dismay.
 她尽管很惊慌，但是还是假装自信。

2.       For such a big sum, we should attach importance to it.
 数目巨大，我们应予以重视。

3.       It's time for you to wake up and attend to your business.
 该是你奋发立业的时候了。

4.       It is preposterous to attribute to mere external conditions.
 仅仅以外部环境解释一切是不合理的。

5.       Don't back away from this chance.
 不要放弃这次机会。

6.       Don't back up the moment you run up against a little difficulty.
 不要遇到一点困难就打退堂鼓。

7.       You can always bank on me if you need money.
 你要是需要钱，可以永远依靠我。

8.       Your argument is based on a set of questionable assumptions.
 你的论证建议在一系列有争议的假设上。

9.       It looks as if we are in for a big storm.
 看来我们一定会遇到一场暴风雨。

10.    He still bore a grudge against her.
 他对她有不满情绪。

11.    Is it hard to bear with criticism?
 忍受批评困难吗？

12.    Do you believe in a cosmic plan?
 你信命吗？

13.    He doesn't belong to the inner circle.
 他不是圈里人。

14.    The room was so hot, and I'd had nothing to eat, so I blacked out.
 屋里太热，我又没吃东西，所以就晕倒了。

15.    We had twelve hours of sunshine yesterday, as against a forecast of continuous rain.
 昨天有12小时的晴天，天气预报却说有雨。

16.    Given that they've inexperienced, they've done a good job.
 在缺乏工作经验的情况下，他们做的已经不错了。

17.    Supposing that she changes her condition in life, what then?
 假设他结婚了，以后会怎样？

18.    I shall go provided that it doesn't rain.
 加入不下雨，我就去。

19.    Suppose he can't come, who will do the work?
 如果他不能来，谁能做这份工作呢？

20.    The government is not wholly to blame for the recession.
 这次衰退不能完全怨政府。



**Daily Accumulation**



# Day 19



1.       Why are you trying to put the blame on me?
 你为什么总想把责任推到我身上呢？

2.       It may well blow up a storm tonight.
 今晚很可能会下一场暴雨。

3.       Girl you are one of a kind and you blow my mind baby.
 你是我请进所思所想的那类女孩。

4.       These facts boil down to very little significance.
 这些事实没有多大意义。

5.       The disquiet will boil over in the long run.
 这种不安情绪终有一天会爆发的。

6.       We must all bow to necessity, i.e. accept what is inevitable.
 对于无可奈何的事，我们要顺其自然。

7.       The company wants to break away from its down market image.
 这个公司想摆脱它面对低消费群体的形象。

8.       We did not expect that you had come into the picture at all.
 我当时没有想到你会有牵连。

9.       Some people were hurrying, as if anxious to get ahead of others.
 有些人匆匆忙忙，似乎着急超越别人。

10.    Some make a virtue out of giving in the sense of a sacrifice.
 有些人认为给予是牺牲，并以此为德行。

11.    The poem was all the rage then.
 这首诗在当时非常流行。

12.    Production of the goods must fit in with the needs of the society.
 商品生产必须符合社会需要。

13.    How much money do you plan to keep in your account?
 你计划在户头上定期存多少钱？

14.    Please pull up a chair and join the conversation.
 请那把椅子来一起聊天。

15.    We're all going to die at some point.
 我们都将在某个时间死去。

16.    A good education gives your child a head start in life.
 良好的教育让你的孩子在人生的起跑线上领先一步。

17.    Her cleverness seems to get in the way of her emotions.
 她好像聪明有余，激情不足。

18.    Nothing about this sum has come to light.
 关于这笔钱，人们一无所知。

19.    I will look back at this one with undiluted pleasure.
 回顾此事，我会无比的开心。

20.    Be assertive and spell out exactly how you feel.
 要自信一点，把自己的感受讲清楚。



**Daily Accumulation**



# Day 20



1.       We must carry on hoping for the best.
 我们必须抱有好的希望。

2.       I didn't think he stepped on your toes on purpose.
 我不认为他是故意踩你脚的。

3.       Solitude is the richness of the soul, loneliness is its poverty.
 孤独是灵魂的财富，寂寞是灵魂的贫瘠。

4.       Your attitude, not your aptitude, will determine your altitude. --Zig Ziglar
 态度而非才能，决定你的人生高度。

5.       Once the aim is fixed, we should not change it arbitrarily.
 目标一旦确定，就不要随意更改。

6.       It's an auspicious year for your career.
 您今年在事业方面运势很旺。

7.       He was deliberate in his speech and action.
 他谨言慎行。

8.       Not to mention that all Englishmen are born heretics.
 且不说英国人天生都是异端分子。

9.       They collect funds to finance the movement.
 他们为这一运动筹集资金。

10.    There are three essentially different ways of tackling the problem.
 有三种完全不同的方法解决这个问题。

11.    She mourned for the beloved past.
 她怀念美好的过去。

12.    At the age of sixty he took up the study of Russian.
 在60岁时，他开始学习俄语。

13.    Break a leg.
 祝好运。

14.    Don't dream away the hours.
 别虚度光阴。

15.    A slow, reminiscent smile spread over her face.
 怀旧的笑慢慢出现在她的脸上。

16.    Hope and energy drain away over the years.
 岁月渐使精力和希望枯竭了。

17.    A good idea came across my mind.
 我突然想到了一个好主意。

18.    She gets various perquisites in addition to her wages.
 她除了工资外，还有各种津贴。

19.    He is, so to speak, a walking dictionary.
 他可谓一部活字典。

20.    Touche!
 讲得好！



**Daily Accumulation**



# Day 21



1.       Retailers will have to mark down prices sharply to bring in sales.
 零售商不得不为了销量而降低价格。

2.       Now we come to the crux of the matter.
 现在我们来谈问题的症结。

3.       We never engage in irregularities.
 我们绝不好歪门邪道。

4.       He regarded his marriage merely as a means to an end.
 他把婚姻当做了达到目的的手段。

5.       While he had originally traveled in order to study, traveling had become an end in itself.
 尽管他开始是为了学习而旅行，后来旅行本身也成为一种享受。

6.       Opinions vary from person to person.
 观点因人而异。

7.       Finally, it is like drawing water with a sieve.
 到头来，竹篮打水一场空。

8.       He ekes out a living with a market stall.
 他靠摆摊勉强糊口。

9.       He is a man who will spoil rather than accomplish things.
 他这个人成事不足败事有余。

10.    We have to resolve this matter once and for all.
 我们必须彻底解决这一问题。

11.    There is no reason why you couldn't live it up once in a while.
 偶尔放纵一下也无妨。

12.    Such a chance comes once in a blue moon.
 这样的机会极其难得。

13.    His remarks doesn't weigh with me at all.
 他说的话对我一点都不重要。

14.    Nothing comes without consequence.
 有因必有果。

15.    It was achieved with minimum fuss and maximum efficiency.
 事半功倍地完成了。

16.    Everything must be carefully examined before we act, then twice as much can be accomplished with half the effort.
 事情要考虑周详再动手，才能事半功倍。

17.    It can't be expressed in a few words.
 一言难尽。

18.    We call meanness nobility and hatred honor.
 我们善恶不明，是非不分。

19.    He started going bald in his twenties.
 他二十几岁开始谢顶。

20.    Be assertive rather than bottle up your anger.
 要坚定自信，而不是把愤怒藏在心底。



**Daily Accumulation**



# Day 22



1.       In Japan firms are biased towards growth rather than profits.
 在日本，公司偏向于发展，而不是追求利润。

2.       Our literature and art ought to cater to popular taste.
 我们的文艺应该被大众所喜闻乐见。

3.       Time is like the water in sponge, if you squeeze, you can always get some.
 时间就像海绵里的水，挤一挤总会有的。

4.       Don't pick me up on words.
 不要挑我的错。

5.       She would cleverly pick up on what I said.
 她会非常聪慧地领悟我所说的话。

6.       It's a bit tricky at first till you get the hang of it.
 刚开始有点难，掌握之后就好了。

7.       The reporters took his speech down in shorthand.
 记者用速记记下了演讲。

8.       Will you take a stab at this problem for me?
 你能试着帮我们解决这个问题吗？

9.       The pillow cover can match up with the sheets.
 这条枕巾可以和床单配上。

10.    Bad deeds, as well as good, may rebound upon the doer.
 善有善报，恶有恶报

11.    We must keep a sense of proportion about all this.
 我们做事应分轻重缓急。

12.    We ought to decide what priority the emergency rated.
 我们应该判断紧急情况的轻重缓急。

13.    However sly a fox may be, it is no match for a good hunter.
 狐狸再狡猾也斗不过好猎手。

14.    The two most important days in your life are the day you were born and the day you find out why.--Mark Twain.
 人生最重要的两天是你出生的那天和你明白人生意义的那天。

15.    Buildings are left to decay at the mercy of vandals and the weather.
 建筑物由于人的破坏和风吹日晒而日益破败。

16.    Let it be.
 顺其自然。

17.    We have to make a small charge for refreshments.
 我们要收取少量茶点费。

18.    How much do you charge for mending a pair of shoes?
 补一双鞋要多少钱？

19.    So many marriages have come to grief over lack of money.
 很多婚姻由于缺钱而以失败告终。

20.    Even in Hollywood, there's no such thing as a free lunch.
 即使在好莱坞也没有免费的午餐。



**Daily Accumulation**



# Day 23



1.       Sap one's spirit by seeking pleasures.
 玩物丧志。

2.       Let me lay it on the line, we have done our share.
 坦率地说，我们已经尽力了。

3.       She was constantly on the phone, lading out inside details to reporters.
 她经常打电话，向记者提供内幕消息。

4.       A mother's instinct care for her young is a powerful drive, there's only one that can be stronger, hunger.
 母亲对子女的照顾是一种强大的本能。只有一种本能强过它，饥饿。

5.       I feel keenly that my ability falls short of my wishes.
 我深感力不从心。

6.       He listened with an absent air and kept glancing at the door.
 他一副心不在焉的样子，不是瞥一眼门。

7.       A storm may arise from a clear sky.
 天有不测风云。

8.       His spirit of assiduous study is worthy of emulation.
 他刻苦钻研的精神值得效仿。

9.       Bulls-eye!
 说中了。

10.    Or it might flip a switch that ups the worms stress resistance.
 它可能触发了虫子的抗逆性。

11.    Road rage.
 路怒。

12.    Bosses who hit the gym tend to be less abusive to their employees.
 常去健身的老板对员工不那么苛刻。

13.    But to get the most bang for their buck, they might want to play their ad right after the game ends--not during it.
 但是为了获得最大利益，他们会在比赛结束时播放广告，而非其中。

14.    They came to the baseball field to root for their school team.
 他们来到棒球场为他们的校队加油。

15.    As long as there's a nail-biter, that final ad may be poised to score a touchdown.
 只要有紧张的局势，最后的广告就等着触地得分时播放。

16.    Confucius says, 'Having heard the Way in the morning, one may die content in the evening.'
 孔子说，朝闻道，夕可死。

17.    Cease to struggle and you cease to live.
 生命不息，奋斗不止。

18.    Apple of my eye.
 掌上明珠。

19.    It is within walking distance of my house.
 从我家步行即可走到。

20.    Take time to get your thoughts into shape.
 从容地清理一下你的思路。



**Daily Accumulation**



# Day 24



1.       Air one's thoughts.
 发表某人的看法。

2.       It's good to suck in such clean fresh air for a change.
 呼吸点新鲜空气真好，换换心情。

3.       The mere mention of an ex-girl friend can turn even the sanest girl into Psycho Chick.
 仅仅提起前女友就能让一个明事理的女孩变成神经病。
 sanest 心智健全的

4.       Its fabulous parks are loved by Londoners all year round.
 它神话般的公园每年都是伦敦人的挚爱。
 all year round 全年

5.       Live under the same roof.
 生活在同一屋檐下。

6.       All hard work brings profit.
 天道酬勤。

7.       Fortune favors the diligent.
 天道酬勤。

8.       That they were in truth sisters was clear from the facial resemblance between them.
 很明显，她们确实亲姐妹，因为脸型很相似。

9.       I always take fuel consumption into consideration when buying a car.
 我买车时，总会考虑耗油量。

10.    When two industrial giants clash, small companies can get caught in the crossfire.
 两个工业巨头的争斗，会殃及池鱼。
 get caught in 陷入；遭受

11.    Each side set forth its position on the question.
 各方阐述了自己对于这一问题的立场。
 set forth 提出；出发

12.    Just play it safe, cover your ass, keep your head down.
 要谨慎行事，保护自己，并保持低调。

13.    He sensibly decided to lie low for a while.
 他明智地决定保持低调。

14.    The wedding will be a very low-key affair.
 婚礼将低调举行。

15.    There's a more concrete gesture of our gratitude.
 我们需要更实在的方式表达感谢。

16.    Am I meant to find that reassuring?
 你这么说我就能安心了吗？
 be meant to 必须；打算；旨在

17.    Family holiday's are meant to be a break from routine.
 家庭假日就是从平淡的日常生活中放松一下。

18.    I suppose asking to borrow a cup of sugar is a step too far?
 我想找您借杯糖是不是有点过分了？
 a step too far 太大的一步；过分

19.    17years, and still evolving with the time remains an entirely foreign concept to you.
 17年过去了，与时俱进对你来讲还是个陌生的概念。

20.    One of these days, I'm gonna smash his face in.
 总有一天，我会打烂他的脸。



**Daily Accumulation**



# Day 25



1.       Your complaint has been duly noted.
 您的投诉已受理。

2.       Would you like a lift home?
 要搭车回家吗？

3.       And if it weren't for his courage, my mistake would have cost the lives of every man present.
 要不是他的勇敢，我的疏忽会让在场的所有人丧命。

4.       I really don't have a beef with Wayne.
 我和韦恩真的没有过节。

5.       As Hemmingway said, there's nothing noble in being superior to your fellow man, true nobility is being superior to your former self.
 海明威说，优于别人并不高贵，真正的高贵是优于昨天的自己。

6.       Let go of me.
 放开我。

7.       Sit on it.
 去你妈的！

8.       I guaran-fucking-tee it.
 我能保证。

9.       Now that we've gone as far as this, we must either go the whole hog or drop it altogether.
 到这步了，我们只好一不做二不休了。
 whole hog 全部地；最大程度

10.    In my line of work.
 干我们这行。

11.    That would be a neat trick.
 这倒是个不错的主意。

12.    I hate to put on airs.
 我不喜欢装腔作势。

13.    No college loans to pay off.
 不需要还大学贷款。

14.    I'm a nasty piece of work.
 我可不是好惹的。

15.    Now, down to business.
 现在说正事。

16.    I didn't have his knack.
 我没有他的天分。

17.    Keep your hair on.
 冷静，别慌。

18.    I don't understand half of what you're saying.
 你说的话我多半都听不明白。

19.    I've got time pressures.
 我很赶时间。

20.    Some stores force up the prices.
 商店抬高物价。
 force up 迫使上升



**Daily Accumulation**



# Day 26



1.       You're a sideshow attraction.
 你不过是一个无足轻重的配角。

2.       Not even slightly.
 一点都不好。

3.       I have been date-mining a whole lot of unexplained phenomenon.
 我一直在收集大量未解现象的数据信息。

4.       And I believe I'll take a pass on that.
 我想我还是敬谢不敏了。

5.       That makes the two of us.
 那咱两想到一块去了。

6.       Live in the now.
 活在当下。

7.       She's on fire tonight.
 她今晚真是饥渴。

8.       Did you get it off the internet.
 你是在网上找到的吗？

9.       There's no point wondering.
 想是没有意义的。

10.    To lock eyes with your future.
 凝视你的未来。

11.    John went ahead on his own.
 约翰一意孤行。

12.    Guess we're not all cut from the same cloth.
 看来我们并不是同一类人。

13.    Kind of the point, mate.
 正合我意。

14.    You fail to grasp the stakes here, John.
 你没明白重点。

15.    And you call me a con man.
 你还说我是骗子呢。

16.    Pick up the tab, will you?
 酒钱你付了。

17.    It's a nice place, luv, but I think I'll sit this one out.
 高明的一招，亲爱的，但是我要走了。
 sit out 耐心听完；袖手旁观

18.    So what's your take on what happened?
 你怎么看待发生的这件事？

19.    There's no need to cause a fuss, all right?
 没有必要大动干戈，好吗？

20.    A reason to elevate my personal security.
 看来我要更注意个人安全了。



**Daily Accumulation**



# Day 27



1.       We're gonna get to the bottom of this.
 然后咱们一起查他个水落石出。

2.       I used to hear in a month of Sundays.
 我过去很长的时间内听到的。

3.       Faith is an unshakable acceptance with no room for doubt.
 信仰是毋庸置疑且不可动摇的笃信。

4.       So, what’s a chica bonita in her mid-20s doing slumming it in a one-horse Welsh mining town in the middle of Pennsylvania?
 为什么一个20多岁的美丽姑娘在宾夕法尼亚中部的一个偏僻的威尔士矿山镇上干什么呢？

5.       It won’t be bad memories nipping at your heels but the scourge of hell gunning for your soul.
 那不会是对你穷追不舍的糟糕回忆，而是地狱的恶魔谋求你的灵魂。

6.       If not, then we go our separate ways.
 如果不行，我们就各走各的路。

7.       All right, look, if you’re just gonna fly off the handle and turn this into some kind of a spook show, then I have nothing to say to you right now.
 好，如果你非要失去理智，把这说成什么幽灵作怪的话，那我对你没什么可说的。

8.       The church that Ellis turned his back on.
 那就是艾利斯遗弃的教堂。

9.       I’m taking you for a spin.
 我带你兜个风。

10.    She’s having a bit of a turn.
 她有点不舒服。

11.    Everyone who puts their trust in me dies.
 所有信任我的人都会死。

12.    It's of no consequence.
 这不碍事。

13.    Come off it!
 别骗人了。

14.    Bumper-to-bumper traffic already on the Golden State Freeway.
 金州高速已经堵得动弹不得。

15.    It is in your moments of decision that your destiny is shaped.Tony Robbins
 在你做决定的那一刻，你的命运就已经注定。托尼.罗宾斯（美国演说家）

16.    Yeah. So, what's the take-away?
 是啊，所以结论是什么？

17.    Maybe the events in our lives were **set in motion** a long time ago. There's an old Buddhist saying that, When you meet your soulmate, remember that the act to bring you together was 500 years **in the making**. So always appreciate and be kind to each other.
 也许在我们生命中发生的事情是很久前就种下因。佛教有句老话，今生你与伴侣的相遇，是因为你们两个人五百年的一次回眸。所以要时刻善待彼此，心怀感激。

18.    with all due respect
 恕我直言

19.    Well, you certainly went to great length to do it.
 为此你还真是不留余力。

20.    You weren’t flat-lining, Barry.
 你的心脏并没有停止。



**Daily Accumulation**



# Day 28



1.       They've really built up this area. All the trees have gone.
 这个地区已经是建筑林立，原来的树都没有了。
 build up 建筑林立；build-up 布满建筑的

2.       Our host suggested some games to try and liven up the party.
 主人建议玩些游戏，活跃一下晚会气氛。
 liven up 使气氛活跃起来

3.       The price of petrol has shot up recently.
 汽油价格近期骤然飙升
 shoot up 骤然升高

4.       How long will it take for this cut to heal up?
 这个伤口需要多久才能愈合？
 heal up 康复；愈合

5.       I'm saving up to buy a new motorbike.
 我在存钱想买个新摩托车。
 save up 攒钱

6.       You'll have to tidy up this room before your father gets home.
 你得在你父亲回家之前将房间整理干净。
 tidy up 使（房间）干净整洁

7.       I was brought up by my grandparents from the age of five.
 我从五岁起就由祖父母一手带大。
 bring up 抚养大

8.       I went on a course to brush up my Spanish.
 我参加了一门课程去温习西班牙语。
 brush up 温习

9.       Look up the meaning of the word in this dictionary.
 请在词典中查找该词的意思。
 look up 查找

10.    I would have called you up but I didn't know your number.
 要是知道你的电话号码，我早就给你打电话了。
 call up 给某人打电话。

11.    A problem has cropped up so I may be working all night.
 问题意外地出现了，所以我可能要整晚工作。
 crop up （意外地）出现或发生

12.    If Cathy hadn't owned up, I might have been punished instead.
 假如不是凯西主动认错，那么被惩罚的可能是我。
 own up 承认有错；坦白

13.    The van pulled up outside the front door.
 货车在大门外停了下来。
 pull up (of a vehicle) （车辆）减速并停下

14.    I want you to split up into groups of four.
 我想将你们分成四人一组。
 split up 分成组

15.    I was very angry but I tried to bottle up my feelings.
 我当时很气愤，但是我已经尽力控制自己了。
 bottle up 抑制（情感）

16.    The reporters were trying to dig up information about her.
 记者们设法打探出有关她的消息。
 dig up 找出（情报等）；揭露秘密

17.    The bathroom's upstairs if you'd like to freshen up.
 如果你想梳理一下，卫生间在楼上。
 freshen up 梳洗打扮

18.    She screwed up her face in a look of intense disapproval.
 她面部扭曲，一脸的不赞同。
 screw up one's face 面部扭曲

19.    This project is secret so we'll have to tighten up security.
 这个项目是机密，我们必须加安保工作。
 tighten up 强化（安全）

20.    The party finally broke up at midnight.
 晚会终于在半夜结束了。
 break up (of a party, meeting) （聚会，会议）结束



**Daily Accumulation**



# Day 29



1.       Do up your coat or you'll catch cold!
 系上大衣，不然你会着凉的。
 do up 系上

2.       The car drew up and the driver opened his door.
 那辆车停了下来，接着，司机打开了门。
 draw up (of a vehicle) （指车辆）停下来

3.       He folded up the document and put it carefully away.
 他卷起了文件，并将其小心地放好。
 fold up 卷起

4.       That's the second time the bank's been held up this year.
 这已经是这个银行今年第二次遭到抢劫了。
 hold up 遭劫

5.       To keep up appearances they still go to the best restaurants.
 为了保全面子，他们仍然进最好的餐馆。
 keep up (appearances) 维持（面子）

6.       Mum will kick up a fuss if everything isn't neat and tidy.
 如果东西摆放不整齐的话，妈妈就会小题大做。
 kick up a fuss 大吵大闹；小题大做

7.       Could all the contestants line up in front of the judges?
 请所有参赛者列队站在评委面前，好吗？
 line up 排列起来

8.       The windows misted up and we couldn't see outside.
 窗户上蒙上了一层水汽，外面什么也看不见。
 mist up 蒙上一层水汽

9.       Work really piled up when I was away on business.
 在我出差的过程中，工作可是积了一大堆。
 pile up (of work) （工作）堆积

10.    I'll try to sum up the situation in a few words.
 我将尽量用几句话来概括一下形势。
 sum up 概括；总结

11.    Don't turn up the volume! It's loud enough already.
 不要把音量调大！已经够大了。
 turn up 放大（音量）

12.    An opportunity for extra work has come up.
 一个额外的工作机会来了。
 come up 出现；发生

13.    Prices have gone up by five percent this year.
 物价今年升高了百分之五。
 go up 升高

14.    I picked up some of my English by listening to pop music.
 我通过听流行音乐轻松学会了一些英语。
 pick up 轻松地学到

15.    The hotels were full so my friends put me up.
 所有的宾馆已客满，因此朋友接待了我。
 put up 向某人提供食宿

16.    The skirt was too long so she had to take it up.
 裙子太长了，因此她把它裁短了。
 take up 裁短

17.    I feel tired so I'm going to lie down for a bit.
 我累了，我得去躺一会而。
 lie down 躺下

18.    The soup's too hot. I'll leave it to cool down.
 汤太热了，我等它凉一凉。
 cool down （温度）慢慢降下来

19.    You eat too many sweets. You should try to cut down.
 你吃糖吃的太多了，应尽量少吃点儿。
 cut down 减少食量

20.    Prices have been marked down an extra ten percent.
 价格有额外降了百分之十。
 mark down 减价



**Daily Accumulation**



# Day 30



1.       When the crowd had quietened down, he continued speaking.
 人群安静下来以后，他接着继续讲。
 quieten down 安静下来

2.       They tore down the old posters and put up new ones.
 他们将旧海报撕下，换上了新的。
 tear down 撕下

3.       I had to tone down the speech to avoid a diplomatic incident.
 为了避免这次外交事件，我只得将言辞变得含蓄些。
 tone down （语气）缓和下来

4.       We had to bed down in the stables for the night.
 我们不得不在马厩中将就一夜。
 bed down （将就）过夜

5.       Her boss had to climb down and admit he had misjudged her.
 他的老板勉强认了错，承认他错怪了她。
 climb down 认错

6.       This necklace has been handed down from mother to daughter.
 这个项链是由母亲传给女儿的。
 hand down 传给下一代

7.       It was such a stupid thing to do. I'll never live it down.
 不要着急，我会来的。我不会让你失望。
 live down 使（人们）淡忘

8.       The lorry ran down an old lady who was crossing the road.
 那辆卡车撞到了一位过路的老妇人。
 run down 撞到

9.       The constant questioning was beginning to wear me down.
 不断的提问开始让我感到疲倦。
 wear down 使疲惫

10.    He was weighed down by all his worries.
 他因焦虑感到沮丧。
 weigh...down 使人感到忧虑或沮丧

11.    I need a relaxing hobby to help me wind down at the weekend.
 我需要一种休闲的活动来使我在周末放松一下。
 wind down 放松

12.    Please copy down what I've written on the board.
 请将我写在黑板上的抄下来。
 copy down 抄下来

13.    I don't want to be tied down by a wife and a family.
 我不想被婚姻和家庭所困。
 tie down 限制自由

14.    The dog knocked a tin and sent the rest tumbling down.
 狗撞到了一听罐头，使得其余的罐头滚了一地。
 tumble down （无序地）倒下；倒塌

15.    You must follow the procedure I have laid down.
 你应该按照我订立的程序办事。
 lay down 立下制度，规矩

16.    We've narrowed down the choice to Edinburgh or Athens.
 我们已将选择缩小在爱丁堡和雅典两地。
 narrow down 缩小（范围）；减少（数量，长度等）

17.    The authorities tried to play down the incident to prevent panic.
 当局试图弱化这件事以防止出现恐慌。
 play down 减弱某事的重要性

18.    As night came they scaled down the rescue operation.
 随着夜色的降临，他们缩小了营救范围。
 scale down 缩小（范围）

19.    It was midday and the sun was beating down.
 当时正值正午，烈日当空，骄阳似火。
 beat down （只太阳）曝晒

20.    When the noise had died down, the chairman went on speaking.
 喧闹声消失后，会议主席接着往下说。
 die down 减弱



**Daily Accumulation**



# Day 31



1.       The rain was pelting down outside so I decided to stay in.
 外面雨下得很大，因此，我决定呆在家里。
 pelt down （指雨）倾盆而下

2.       If the sun is too hot, pull down the blind.
 如果太阳晒得厉害，就把窗帘拉下来。
 pull down 拉下来

3.       The council has set down standards of hygiene for restaurants.
 委员会已经订立了餐馆卫生标准。
 set down (standards) 订立标准

4.       Troublemakers in the audience tried to shout the speaker down.
 听众中滋事者试图高声呼喊来扰乱讲话人。
 shout down 大声喊叫以阻止某人说话

5.       Tucker slammed down the phone and swore under his breath.
 塔克用力将电话扔下，并低声咒骂着。
 slam down 用力摔，扔，推等。

6.       I've finally managed to track down a copy of the book.
 我最终设法找到了一本那样的书。
 track down 搜寻，追踪

7.       Why don't you come down and see us some time?
 有空来我们这里玩好吗？
 come down （从北方）南下；（从城镇）到乡下

8.       Our cat was so ill that we had to have him put down.
 我们家的猫病得很厉害，我们忍痛将它弄死了。
 put down 将其杀死

9.       The reporter took down the details of the accident.
 记者记下了事故的详细情况。
 take down 记下

10.    The boss turned down my request for a day off.
 老板拒绝了我请假一天的要求。
 turn down 拒绝接受

11.    I'm trying to cut out any desserts with a high sugar content.
 我在竭力戒食一切高糖甜点。
 cut out 戒食（某物）

12.    I just can't squeeze out that last bit of toothpaste!
 我就是没办法挤出最后那点牙膏。
 squeeze out 挤出

13.    Half the audience walked out because the acting was so bad.
 半数观众离开了，因为表演太差了。
 walk out 离开以示抗议

14.    We have to check out by eleven at the latest.
 我们最迟要在11点半办理结账手续。
 check out 办理退房或结账手续

15.    We had to leave out our best player because of injury.
 我们不得不把最好的运动员排除在外，他们因伤未能出赛。
 leave out 排除在外

16.    Try to pick out the smaller tomatoes. They're sweeter.
 挑选小一些的西红柿，小的甜些。
 pick out 挑选

17.    The shopkeeper reached out and took a tin from the shelf.
 店主伸手从货架上取了一听罐头。
 reach out 伸手够东西

18.    The pianist has backed out so I've got to find another one.
 该钢琴家未履行承诺，因此，我只得找了另一位。
 back out 毁约

19.    I need a ruler to cross out this mistake.
 我需要一把尺子，将错误划掉。
 cross out 划掉（错误）

20.    He poured out his problems to his best friend.
 他像好朋友尽情地诉说自己的难事。
 pour out 尽情倾诉（29）
