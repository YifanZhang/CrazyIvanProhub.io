---
layout:     post
title:      "[TFL] Speaking I"
subtitle:   "Q1 Q2 应试"
date:       2008-01-01 12:00:00
author:     "CrazyIvan"
header-img: "img/post-bg-apple-event-2015.jpg"
catalog:    true
tags:
    - TOEFL
---
# 1. Money

![](/img/toefl-q1-q2-jdk/Image_001.jpg)

+ (It is a good chance to...) Money doesn't grow on trees. As for me, I'm a student still relying on parents. I don't want to waste their hard-earned cash. Just as a saying goes, 'the best steel must be used for the blade's edge.' I have to put aside some money for the future, to prepare for a rainy day. (54)



+ 金钱不是白来的。对我而言，我还是个依靠父母的学生。我不想挥霍他们的血汗钱。而且有一句话说得好“好钢用在刀刃上”。我要存些钱为将来打算，未雨绸缪。



# 2. Time-2

![](/img/toefl-q1-q2-jdk/Image_002.jpg)

+ (It is a good chance to...) Time and tide wait for no man. I will cherish every moment to realize my dreams and achieve my goals. Life is to be lived only once. I don't want to regret being mediocre in all my life when I'm about to be six feet under. (46)



+ 时不我与，时不我待。我要珍惜每一刻去实现理想，达成目标。人生只有一次。我不想在临死之前后悔一生的平庸。



# 3. Exercise

![](/img/toefl-q1-q2-jdk/Image_003.jpg)

+ (It is a good chance to...) Life lies in movement. Exercising not only has a positive impact on our mental, as well as physical health, but also gives us a good body shape which helps build up confidence. (32)

+ 生命在于运动。运动不仅会有益于我们精神和肉体上的健康，还能够给我们一个好身材，帮助建立自信。



# 4. Health

![](/img/toefl-q1-q2-jdk/Image_004.jpg)

+ (It is a good chance to...) Wealth is nothing without health. Health is the capital of revolution. Without good health, anything you're pursuing, success, fame or fortune are nothing but illusion.

+ 健康胜于财富。健康是革命的资本。没有良好的健康，一切你所追求的东西，成功或名利不过是镜中花，水中月。



# 5. Relax

![](/img/toefl-q1-q2-jdk/Image_005.jpg)

+ (It is a good chance to...) Excessive stress will not only lower my immunity to common illness like cold and fever, but also wreak havoc on my work productivity. And it is also dangerous to my mental health. I might suffer bipolar disorder. (37)

+ 过度压力不仅会降低我对常见疾病的免疫力，比如感冒发烧。而且还会极大破坏我的工作效率。另外，它也会对我的精神状态造成损害。我很可能会得上躁郁症。


# 6. Friend

![](/img/toefl-q1-q2-jdk/Image_006.jpg)

+ (It is a good chance to...) Life is full of ups and downs. One cannot stand out single-handedly. When we fall on evil days, we will need a shoulder to cry on. (26)



+ 人生充满起落。没有人能够独自成功。当我们遇到人生不如意之时，需要一个肩膀去依靠。



# 7. Knowledge

![](/img/toefl-q1-q2-jdk/Image_007.jpg)

+ (It is a good chance to...) Cease to struggle and you cease to live. There is no other productive force stronger than knowledge. I want to let knowledge be my cynosure to walk by, guide me forward in the dark night, 'cuz I believe knowledge changes fate. (41)

+ 生命不息，奋斗不止。知识是第一生产力。我愿让知识成为我之路的明灯，在黑暗中指引我行进的方向。我相信知识改变命运。



# 8. Convenient

![](/img/toefl-q1-q2-jdk/Image_008.jpg)

+ It shortens the distance between people and provides us (with) convenience. By simply moving your fingers, you can get anything you want in a matter of seconds. (27)

+ 它缩短了人与人之间的距离，提供给我们方便。只需动动手指，你就可以得到一切你想要的。



# 9. failure

![](/img/toefl-q1-q2-jdk/Image_009.jpg)

+ Failure breeds success. It is an inevitable part of change, and in fact it should be celebrated--without failure, we'd learn nothing. So I believe the secret of success is to get up one more time than you fall down. As long as you still hold on to your dream tightly and never let it go, your day will eventually come. (61)

+ 失败是成功之母。他是变化不可或缺的一部分。实际上，失败是应该被庆祝的，因为没有失败，我们就无法进步。因此我相信，成功的秘诀在于站起比跌倒多一次。只要你仍然紧紧地抓住梦想，从未放弃，那么你的成功终将到来。



# 10. Way to study

![](/img/toefl-q1-q2-jdk/Image_010.jpg)

+ I'm always up to my ears in study, have my nose buried in a stack of reference materials round the clock to practice, day by day, year after year. Because I believe a repetition of practices is the only way to gain genuine knowledge. (44)

+ 我学习努力，埋头苦读，日复一日，年复一年。因为我相信，反复练习是获得真知的唯一途径。



# 11. environment

![](/img/toefl-q1-q2-jdk/Image_011.jpg)

+ It'll help reduce traffic emission which contains a lot of air-pollutants like carbon-monoxide, sulfur-dioxide and particulate matters, bring back to us a clear sky. (24)



+ 它能够减少交通尾气排放，其中包含很多空气污染物质，比如一氧化碳，二氧化硫和颗粒物质，还我们一片蓝天。



# 12. food

![](/img/toefl-q1-q2-jdk/Image_001.jpg)

+ Dining out is risky. I would never know what ingredients, or even chemicals they put in my food, like expired meat, hogwash oil or Sudan Red.



+ 外边吃饭有风险。我永远不会知道他们往我的食物里放什么材料，甚至化学品，比如过期肉，地沟油和苏丹红。



+ Pecking duck is my best favor. It is crispy outside and juicy inside, fat but not greasy which will lead you to endless aftertastes.



+ 北京烤鸭是我的最爱。它外焦里嫩，肥而不腻，会让你回味无穷。



+ Steve Jobs was abandoned a baby by his birth parents. When grown up, he dropped out of Reed college due to poor financial condition. He'd borne the greatest suffer in his life, but became a legend in electronics field. Isn't that inspiring? I've learnt from him that a remarkable man despite beginnings.
