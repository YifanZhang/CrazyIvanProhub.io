
# Listening
+ [无老师的Blog](http://www.ibtsat.com)


# Speaking
+ [Q1&Q2 应试](/2008/01/01/toefl-speaking-q1-q2-jdk)

+ [口语强化教材](/img/toefl-speaking-kyqhjc3.pdf)

+ [起承转合词](/img/toefl-writing-special-verb.pdf)

<object data="/img/toefl-writing-special-verb.pdf" type="application/pdf" width="100%" height="1000">
   <p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/img/toefl-writing-special-verb.pdf">Download PDF</a>.</p>
</object>

+ [动植物181题](/img/toefl-speaking-dzw181.pdf)
<object data="/img/toefl-speaking-dzw181.pdf" type="application/pdf" width="100%" height="1000">
   <p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/img/toefl-speaking-dzw181.pdf">Download PDF</a>.</p>
</object>


# Reading



# Writing
+ [特殊背景词汇](/img/toefl-writing-special-verb.pdf)
